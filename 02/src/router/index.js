import Vue from 'vue'
import Router from 'vue-router'
import ListPage from '@/pages/ListPage'
import ItemPage from '@/pages/ItemPage'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'list',
      component: ListPage
    },
    {
      path: '/:item',
      name: 'item',
      component: ItemPage,
      props: { default: true, sidebar: false },
      beforeEnter: (to, from, next) => {
        if (localStorage) {
          let history = localStorage.getItem('viewHistory')

          if (!history) {
            let firstitem = []
            firstitem.push(to.params.item)

            // if var is not exist -> create it
            localStorage.setItem('viewHistory', JSON.stringify({list: firstitem}))
          } else {
            let viewHistory = JSON.parse(history).list

            // store id if it is not in viewHistory
            if (!viewHistory.includes(to.params.item)) {
              viewHistory.push(to.params.item)
              localStorage.setItem('viewHistory', JSON.stringify({list: viewHistory}))
            }
          }
        }
        next()
      }
    }
  ]
})
